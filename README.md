# FORTRAN applications of Nuclear Engineering

A conglomeration of FORTRAN programs which solve Nuclear Engineering Problems (Results of a FORTRAN Course).

Although the source code does not contain compiled programs, compiled files can be found in the [Releases](https://gitlab.com/reeceappling/fortran-applications-of-nuclear-engineering/-/releases)

## Inlabs

1: Calculates molecular density using mass number, mass density, and Avagadro's number.

2: Calculates molecular density using mass number, mass density, and Avagadro's number.

4a: Calculates electron energies at five scattering angles for collisions with a heavy charged particle of known mass and kinetic energy (IF statements).

4b: Calculates electron energies at five scattering angles for collisions with a heavy charged particle of known mass and kinetic energy (SWITCH statements).

6: Given atomic density and decay constant, can calculate timestamped tables of atomic densities of parent and child nuclide over user-defined time ranges

7: Takes a real number, as well as 3 integers from the user to construct 6 arrays

8: Calculates electron energies at variable numbers of scattering angles for collisions with a heavy charged particle of known mass and kinetic energy.

9: Neutron inelastic scattering given input file info, outputs energies to output file

12: compiled external subroutine for Gaussian Elimination, +makefile

## Outlabs

1: Calculates molecular density using mass number, mass density, and Avagadro's number.

2: Calculates electron energies at five scattering angles for collisions with a heavy charged particle of known mass and kinetic energy.

4: Calculates Emergent energy(s) of an inelastic neutron scattering event given initial neutron kinetic energy, nucleus mass number, and scattering angle cosine

6: Approximates SIN(x) by using Taylor series

7: Calculates fluxes and sources at discrete intervals utilizing arrays

8: When user inputs 3 positive integers, arrays and vectors are constructed and the user can select operations to be done with them

9: Calculates populations of 3 isotopes using Finite Differencing in Time.

11: Solves for a solution vector given a tridiagonal matrix and another appropriate vector

12: Uses previously-made (inlab 12) Gaussian Elimination Subroutine to solve 1-Dimensional, single-speed, steady-state diffusion equation

## Midterms

1: Calculates neutron flux and power at 5 given positions on the fuel rod given normalized scalar flux and fuel rod length.

2: Calculates fluxes at radial positions in a bare cylindrical reactor from given data.

## Project

1: Calculating parent and daughter nuclide populations Exactly, or via Finite-Difference Method.

## Final

1: Calculates uncollided dose through a shield

## License
----------------------------------------------------------------------------------------------------------------------------------------------------------------------