PROGRAM APPLING_INLAB4B

	!Author: Reece Appling
	!Date: 2/12/2021
	!Assignment Name: Inlab 4b
	!Assignment Description: Calculates electron energies at five scattering angles for collisions with a heavy charged particle of known mass and kinetic energy.
	
	IMPLICIT NONE
	
	CHARACTER(72)::hcpName
	CHARACTER(1),PARAMETER::v="|"
	CHARACTER(86),PARAMETER::tableHoriz="--------------------------------------------------------------------------------------"
	REAL::hcpKE,hcpMass
	REAL,PARAMETER::me=5.486E-4,PI=3.14159
	!------------------Variable Declaration---------------!
	!CHARACTER VARIABLES
		!hcpName - Holds name of the heavy charged particle
	!CHARACTER PARAMETERS
		!v - Vertical borders and dividers for output table
		!tableHoriz - Horizontal borders and dividers for output table
	!REAL VARIABLES
		!hcpMass - Holds mass (amu) of heavy charged particle
		!hcpKE - Holds kinetic energy (MeV) of heavy charged particle
	!REAL PARAMETERS
		!me - Mass of an Electron (amu)
		!PI - Mathematical constant PI
	!---------------End Variable Declaration--------------!


	!Prompt user to enter the name of the heavy charged particle
	WRITE(*,*) "Enter type of heavy charged particle:"
	READ(*,'(A72)') hcpName !Read name of heavy charged particle from user

	!Prompt user to input the kinetic energy of the heavy charged particle
	WRITE(*,*) "Enter kinetic energy of heavy charged particle (MeV): "
	READ(*,*) hcpKE !Read kinetic energy of the heavy charged particle from user
	
	!SELECT statement to determine heavy charged particle mass, or if the name is not valid
	SELECT CASE(hcpName)
		CASE("Proton")
			hcpMass = 1.007
		CASE("Deuteron")
			hcpMass = 2.014
		CASE("Triton")
			hcpMass = 3.016
		CASE("Alpha")
			hcpMass = 4.003
		CASE("Lithium")
			hcpMass = 7.016
		CASE DEFAULT !name is not valid, show error and terminate program
			WRITE(*,'(A33,A72,A40)') "Error: You entered particle type ",hcpName,", which is not included in the code data"
			WRITE(*,*) "Permissible types (case sensitive) are: Proton, Deuteron, Triton, Alpha, Lithium"
			STOP
	END SELECT
	
	!Draw output table head
	WRITE(*,"(/,A66)")tableHoriz !Draw top horizontal border
	WRITE(*,"(A,2X,A,T41,A,2X,A,T66,A)")v,"Electron Scattering angle (Radians)",v,"Electron Energy (MeV)",v
	WRITE(*,"(A66)")tableHoriz !Draw horizontal line below headers
	
	
	!Draw table body (data rows in table for each of the 5 angles, then the bottom border)
	372 FORMAT (A,4X,A,T41,A,3X,E11.4,T66,A) !Defines format for output table rows
	WRITE(*,372)v,"0",v,((4*me/hcpMass)*hcpKE*(COS(0.0)**2)),v
	WRITE(*,372)v,"pi/4",v,(4*me/hcpMass)*hcpKE*(COS(PI/4)**2),v
	WRITE(*,372)v,"pi/2",v,(4*me/hcpMass)*hcpKE*(COS(PI/2)**2),v
	WRITE(*,372)v,"3 pi/4",v,(4*me/hcpMass)*hcpKE*(COS(3*PI/4)**2),v
	WRITE(*,372)v,"pi",v,(4*me/hcpMass)*hcpKE*(COS(PI)**2),v
	WRITE(*,"(A66)")tableHoriz !Draw bottom horizontal border
END PROGRAM APPLING_INLAB4B