PROGRAM APPLING_INLAB8

	!Author: Reece Appling
	!Date: 4/05/2021
	!Assignment Name: Inlab 8
	!Description: Calculates electron energies at variable numbers of scattering angles for collisions with a heavy charged particle of known mass and kinetic energy.

	IMPLICIT NONE
	
	CHARACTER(72)::hcpName
	INTEGER::angles
	REAL::hcpKE,hcpMass
	REAL,ALLOCATABLE::theta(:),KE(:)
	REAL,EXTERNAL::MASS
	!------------------Variable Declaration---------------!
	!CHARACTER VARIABLES
		!hcpName - Holds name of the heavy charged particle
	!INTEGER VARIABLES
		!angles - number of angles used to cover the range 0 to pi
	!REAL VARIABLES
		!hcpMass - Holds mass (amu) of heavy charged particle
		!hcpKE - Holds kinetic energy (MeV) of heavy charged particle
		!theta - array of angles
		!KE - array of Kinetic Energies
	!REAL FUNCTIONS
		!MASS - finds hcpMass given hcpName or in the event of an error returns -1
	!---------------End Variable Declaration--------------!
	
	!Get all inputs from user and ensure their validity
	CALL PROCESSINPUTS(hcpName,hcpKE,angles)
	
	!Allocate arrays for number of angles
	ALLOCATE(theta(angles),KE(angles))
	
	!Calculate hcpMass using function
	hcpMass = MASS(hcpName)
	
	!Calculate all Angles and KEs
	CALL CALCULATE(angles,hcpKE,hcpMass,theta,KE)
	
	!Output data in table format
	CALL OUTPUTTABLE(angles,theta,KE)
END PROGRAM APPLING_INLAB8

SUBROUTINE PROCESSINPUTS(hcpName,hcpKE,angles)
	IMPLICIT NONE
	CHARACTER(72),INTENT(OUT)::hcpName
	REAL,INTENT(OUT)::hcpKE
	INTEGER,INTENT(OUT)::angles
	REAL,EXTERNAL::MASS
	INTEGER:: errs=0
	!------------------Variable Declaration---------------!
	!CHARACTER VARIABLES
		!hcpName - Holds name of the heavy charged particle
	!CHARACTER PARAMETERS
		!v - Vertical borders and dividers for output table
		!tableHoriz - Horizontal borders and dividers for output table
	!INTEGER VARIABLES
		!angles - number of angles used to cover the range 0 to pi
		!errs - number of found errors (begins at 0)
	!REAL VARIABLES
		!hcpKE - Holds kinetic energy (MeV) of heavy charged particle
	!REAL FUNCTIONS
		!MASS - finds hcpMass given hcpName or in the event of an error returns -1
	!---------------End Variable Declaration--------------!
	
	!Prompt user to enter the name of the heavy charged particle
	WRITE(*,*) "Enter type of heavy charged particle:"
	READ(*,'(A72)') hcpName !Read name of heavy charged particle from user

	!Prompt user to input the kinetic energy of the heavy charged particle
	WRITE(*,*) "Enter kinetic energy of heavy charged particle (MeV): "
	READ(*,*) hcpKE !Read kinetic energy of the heavy charged particle from user
	
	!Prompt user to input number of angles
	WRITE(*,*) "Enter the total number of angles: "
	READ(*,*) angles
	
	!check number of angles
	IF(angles<1) THEN
		WRITE(*,*)"Error: Total number of angles must be greater than 0"
		errs=errs+1
	END IF
	
	!Check hcpKE
	IF(hcpKE<0) THEN
		WRITE(*,*)"Error: Heavy Charged Particle Kinetic energy cannot be less than 0"
		errs=errs+1
	END IF
	!Ensure particle is valid, (mass set to -1 if invalid)
	IF(MASS(hcpName)<0) errs=errs+1
	
	!Write program info
	WRITE(*,"(A)")"Scattering of heavy charged particle by an electron: programmed by Reece Appling"
	
	!Echo input info
	WRITE(*,'(3X,A,T34,2A)')"Charaged Particle Type","=  ",hcpName
	WRITE(*,'(3X,A,T34,A,F9.5)')"Charaged Particle Energy (MeV)","=",hcpKE
	WRITE(*,'(3X,A,T34,A,I4)')"Total number of angles","=",angles
	
	!If there were any errors, warn and terminate
	IF(errs>0) THEN
		WRITE(*,'(A,I2,A)')"There are ",errs," input errors. Please enter any key to terminate"
		READ(*,*)
		STOP
	END IF
	
	!If there are no errors, continue
	WRITE(*,'(3X,A,T34,A,F9.5,/)')"Charaged Particle Mass (amu)","=",hcpKE
END SUBROUTINE PROCESSINPUTS


!Function to find mass of a heavy charged particle given its name,
	!If the name is not hard coded, the mass is set to -1 to trigger errors
REAL FUNCTION MASS(hcpName)
	IMPLICIT NONE
	CHARACTER(72)::hcpName
	!IF statements to determine heavy charged particle mass, or if the name is not valid
	IF(hcpName=="Proton") THEN
		MASS = 1.007
	ELSE IF(hcpName=="Deuteron") THEN
		MASS = 2.014
	ELSE IF(hcpName=="Triton") THEN
		MASS = 3.016
	ELSE IF(hcpName=="Alpha") THEN
		MASS = 4.003
	ELSE IF(hcpName=="Lithium") THEN
		MASS = 7.016
	ELSE !name is not valid, show error
		WRITE(*,'(2A)') "Error: You entered particle type ",hcpName
		WRITE(*,'(5X,A,2/)') "Permissible types (case sensitive) are: Proton, Deuteron, Triton, Alpha, Lithium"
		MASS = -1 !set mass to -1 if invalid
	END IF
END FUNCTION


!Calculates all angles and energies
SUBROUTINE CALCULATE(angles,hcpKE,hcpMass,theta,KE)
	IMPLICIT NONE
	INTEGER,INTENT(IN)::angles
	REAL,INTENT(IN)::hcpKE,hcpMass
	REAL,INTENT(OUT)::theta(angles),KE(angles)
	REAL::me=5.486E-4,PI=3.14159,dtheta
	INTEGER::i
	!------------------Variable Declaration---------------!
	!INTEGER VARIABLES
		!angles - number of angles used to cover the range 0 to pi
		!i - variable used for index of iteration
	!REAL VARIABLES
		!hcpMass - Holds mass (amu) of heavy charged particle
		!hcpKE - Holds kinetic energy (MeV) of heavy charged particle
		!theta - array of angles
		!KE - array of Kinetic Energies
		!me - Mass of an Electron (amu)
		!PI - Mathematical constant PI
	!---------------End Variable Declaration--------------!
	
	!Set dtheta as the angle increment
	dtheta=PI/(angles-1)
	!For each angle increment
	DO i=1,angles
		!Calculate the angle(starting at i=1 is 0)
		theta(i)=dtheta*(i-1)
		!Calculate the Kinetic Energy of the Electron based on the calculated angle
		KE(i)=((4*me/hcpMass)*hcpKE*(COS(theta(i))**2))
	END DO
END SUBROUTINE CALCULATE


!Outputs final table of angles and energies
SUBROUTINE OUTPUTTABLE(angles,theta,KE)
	IMPLICIT NONE
	INTEGER,INTENT(IN)::angles
	REAL,INTENT(IN)::theta(angles),KE(angles)
	CHARACTER(1)::v="|"
	CHARACTER(86)::tableHoriz="--------------------------------------------------------------------------------------"
	INTEGER::i
	!------------------Variable Declaration---------------!
	!CHARACTER VARIABLES
		!v - Vertical borders and dividers for output table
		!tableHoriz - Horizontal borders and dividers for output table
	!INTEGER VARIABLES
		!angles - number of angles used to cover the range 0 to pi
		!i - variable used for index of iteration
	!REAL VARIABLES
		!theta - array of angles
		!KE - array of Kinetic Energies
	!---------------End Variable Declaration--------------!
	
	!Draw output table head
	WRITE(*,"(/,A66)")tableHoriz !Draw top horizontal border
	WRITE(*,"(A,2X,A,T41,A,2X,A,T66,A)")v,"Electron Scattering angle (Radians)",v,"Electron Energy (MeV)",v
	WRITE(*,"(A66)")tableHoriz !Draw horizontal line below headers
	
	!Draw table body (data rows in table for each of the angles, then the bottom border)
	372 FORMAT (A,4X,E11.4,T41,A,3X,E11.4,T66,A) !Defines format for output table rows
	DO i=1,angles
		WRITE(*,372)v,theta(i),v,KE(i),v
	END DO
	WRITE(*,"(A66)")tableHoriz !Draw bottom horizontal border
END SUBROUTINE OUTPUTTABLE