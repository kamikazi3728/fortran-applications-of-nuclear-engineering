PROGRAM APPLING_INLAB9

	!Author: Reece Appling
	!Date: 04/07/2021
	!Assignment Name: Inlab 9
	!Description: Neutron inelastic scattering given input file info, outputs energies to output file

	IMPLICIT NONE
	
	CHARACTER(60),PARAMETER::line='============================================================'
	CHARACTER(70)::title,thermic
	CHARACTER(2)::symbol
	INTEGER::A,n
	REAL::Q,omega,low,high
	REAL,ALLOCATABLE::Ei(:,:)
	!------------------Variable Declaration---------------!
	!CHARACTER PARAMETERS
		!line - Horizontal borders and dividers
	!CHARACTER VARIABLES
		!title - Case title
		!thermic - endothermic or exothermic
		!symbol - Atomic symbol for case nuclide
	!INTEGER VARIABLES
		!A - holds atomic number for case (amu)
		!n - number of steps to consider
	!REAL VARIABLES
		!Q - Q value (MeV) of reaction
		!omega - cosine of the scattering angle
		!low - lower bound of scattering energy to consider
		!high - higher bound of scattering energy to consider
		!Ei(:,3) initial, and possible resultant energies for all steps
	!---------------End Variable Declaration--------------!
	
	!Get user inputs and process them
	CALL PROCESSINPUTS(title,symbol,A,thermic,Q,omega,n,low,high)

	!Allocate all necessary dynamic arrays
	ALLOCATE(Ei(n+1,3))
	
	!Calculate scatter energies
	CALL CALCENERGY(low,high,n,Ei,omega,REAL(A),Q)
	
	!Output data to file
	CALL OUTPUT(Ei,n,title,thermic,symbol,A,Q,omega)
	
END PROGRAM APPLING_INLAB9

SUBROUTINE PROCESSINPUTS(title,symbol,A,thermic,Q,omega,n,low,high)
	IMPLICIT NONE
	CHARACTER(2),INTENT(OUT)::symbol
	CHARACTER(70),INTENT(OUT)::title,thermic
	INTEGER,INTENT(OUT)::A,n
	REAL,INTENT(OUT)::Q,omega,low,high
	!CHARACTER(70)::fileIn,fileOut
	INTEGER::ioin,ioout,nerr
	!Ask for file names
	!WRITE(*,*)"What is the name of the input file?"
	!READ(*,*)fileIn
	!WRITE(*,*)"What is the name of the output file?"
	!READ(*,*)fileOut
	!Open input and output files
	OPEN(UNIT=11,FILE='in9.txt',STATUS='OLD',ACTION='READ',IOSTAT=ioin)
	OPEN(UNIT=12,FILE='out9.txt',STATUS='REPLACE',ACTION='WRITE',IOSTAT=ioout)
	!Handle file open errors
	IF (ioin .NE. 0) STOP 'error opening input file'
	IF (ioout .NE. 0) STOP 'error opening output file'
	
	!Read title line
	READ(11,'(A)')title
	!Read nucleus info
	READ(11,'(A2,I3)')symbol,A
	!Read thermic and Q value
	READ(11,*)thermic,Q
	!Read cosiner value
	READ(11,*)omega
	!Read steps high and low
	READ(11,*)n,low,high
	
	!Verify all inputs
	!Verify bounds are in correct order
	IF(low>=high) THEN
		nerr=nerr+1
		WRITE(*,*)"ERROR: low>=high"
	END IF
	IF(low<=0) THEN
		nerr=nerr+1
		WRITE(*,*)"ERROR: low<=0"
	END IF
	IF(high<=0) THEN
		nerr=nerr+1
		WRITE(*,*)"ERROR: high<=0"
	END IF
	IF((thermic.EQ."exothermic").AND.(Q<0)) WRITE(*,*)"Warning: The input data shows this reaction is exothermic but Q<0"
	IF((thermic.EQ."endothermic").AND.(Q>0)) WRITE(*,*)"Warning: The input data shows this reaction is endothermic but Q>0"
	!n must be positive
	IF(n<=0) THEN
		nerr=nerr+1
		WRITE(*,*)"ERROR: n must be positive!"
	END IF
	!Ensure A is positive
	IF(A<=0) THEN
		WRITE(*,*)"ERROR: Mass number must be a positive integer"
		nerr=nerr+1
	END IF
	
	!Ensure scattering cosine magnitude less than 1
	IF(omega**2>1) THEN
		nerr=nerr+1
		WRITE(*,*)"ERROR: Omega squared is greater than 1!"
	END IF
	
	!If input errors, terminate
	IF(nerr>0) THEN
		WRITE(*,'(I2,A)')nerr," errors detected, terminating program"
		STOP
	END IF
	!If no errors then congratulate
	WRITE(*,*)"Congratulations, all inputs are valid. Continuing execution"
END SUBROUTINE PROCESSINPUTS


SUBROUTINE CALCENERGY(low,high,n,Ei,omega,A,Q)
	IMPLICIT NONE
	REAL,INTENT(IN)::low,high,omega,A,Q
	INTEGER,INTENT(IN)::n
	REAL,INTENT(OUT)::Ei(n+1,3)
	REAL::dE,termOne,termTwo,posSoln,negSoln
	INTEGER::i
	
	dE=(high-low)/(n)
	DO i=1,n+1
		Ei(i,1)=low+(dE*(i-1))
		termOne=(omega/(A+1))*SQRT(Ei(i,1))
		termTwo=((omega/(A+1))**2)*Ei(i,1)
		termTwo=termTwo+((A-1)*Ei(i,1)/(A+1))
		termTwo=termTwo+((A*Q)/(A+1))
		!If reaction is not permissible, set values to 0
		IF(termTwo<0) THEN
			Ei(i,2)=0
			Ei(i,3)=0
		ELSE !Reaction is permissible, calculate solution variables (sqrt of answer(s)) and continue
			posSoln=termOne+SQRT(termTwo)
			negSoln=termOne-SQRT(termTwo)
			IF(negSoln<0) THEN !If the negative solution is less than 0, only the positive solution is valid, set first realistically but second to 0
				Ei(i,2)=posSoln**2
				Ei(i,3)=0
			ELSE !Otherwise Both solutions are valid, save both
				Ei(i,2)=posSoln**2
				Ei(i,3)=negSoln**2
			END IF
		END IF
	END DO
END SUBROUTINE CALCENERGY


SUBROUTINE OUTPUT(Ei,n,title,thermic,symbol,A,Q,omega)
	IMPLICIT NONE
	INTEGER,INTENT(IN)::A,n
	REAL,INTENT(IN)::Ei(n+1,3),Q,omega
	CHARACTER(70),INTENT(IN)::title,thermic
	CHARACTER(2),INTENT(IN)::symbol
	INTEGER::i
	
	!WRITE(12,'(/,A,2/)') "Inlab 9, developed by Reece Appling on Apr 7th 2021"
	WRITE(12,'(/,A,2/)') "Neutron Inelastic Scattering: programmed by Reece Appling"
	
	!Write title
	WRITE(12,'(A,2/)')title
	!Write input data
	WRITE(12,'(A,T40,A,A2,I3)')"Target Nucleus","= ",symbol,A
	WRITE(12,'(A,T40,2A)')"Reaction Type","= ",thermic
	WRITE(12,'(A,T40,A,E12.6)')"Reaction Q value (MeV)","= ",Q
	WRITE(12,'(A,T40,A,F9.5)')"Scattering angle cosine in lab system","= ",omega
	!Write table headers
	WRITE(12,'(2/,A,T20,A,T40,A)')"Ei (MeV)","Eo1 (MeV)","Eo2 (MeV)"
	!Write table rows
	DO i=1,n+1
		WRITE(12,'(E11.5)',ADVANCE='NO')Ei(i,1)
		IF(Ei(i,2)>0) THEN
			WRITE(12,'(8X,E11.5)',ADVANCE='NO')Ei(i,2)
			IF(Ei(i,3)>0) THEN
				WRITE(12,'(8X,E11.5)',ADVANCE='NO')Ei(i,3)
			END IF
		END IF
		WRITE(12,'(/)',ADVANCE='NO')
	END DO
	WRITE(12,*)"Program executed and terminated successfully"
	WRITE(*,*)"Program executed and terminated successfully"
END SUBROUTINE OUTPUT