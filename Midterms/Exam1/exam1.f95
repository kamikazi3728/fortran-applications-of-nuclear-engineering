PROGRAM APPLING_EXAM1

	!Author: Reece Appling
	!Date: 1/20/2021
	!Assignment Name: Exam 1
	!Assignment Description: Given normalized scalar flux and fuel rod length, calculates flux and power at 5 given positions on the fuel rod


	IMPLICIT NONE
	
	CHARACTER(60),PARAMETER::line='============================================================'
	INTEGER::hInt,z1int,z2int,z3int,z4int,z5int,nerr=0
	REAL::flux,H,z1,z2,z3,z4,z5,f1,f2,f3,f4,f5,E1,E2,E3,E4,E5,p1,p2,p3,p4,p5
	REAL,PARAMETER::k=3.1E-11,PI=3.1415926
	!------------------Variable Declaration---------------!
	!CHARACTER PARAMETERS
		!line - Horizontal borders for tables
	!INTEGER VARIABLES
		!hInt - Holds fuel rod length (cm)
		!z1int-z5int - Hold respective values for position along the fuel rod (cm)
		!nerr - number of errors found in input information, starts at 0
	!REAL VARIABLES
		!flux - Holds normalized scalar flux in cm^-2 s^-1
		!H - Holds fuel rod length (cm), REAL equivalent of hInt
		!z1-z5 - REAL equivalents of z1int-z5int respectively
		!f1-f5 - flux in cm^-2 s^-1 at positions z1-z5 respectively
		!E1-E5 - macroscopic fission cross-sections (/cm2 s), at positions z1-z5 respectively
		!p1-p5 - Powers in W/cm3 at positions z1-z5 respectively
	!REAL PARAMETERS
		!k - J/fission constant
		!PI - Mathematical constant PI
	!---------------End Variable Declaration--------------!


	!Prompt user to enter length of fuel rod (cm)
	WRITE(*,*) "Please enter length of fuel rod in cm: "
	READ(*,*) hInt !Read length of fuel rod (cm) from user
	H = REAL(hInt) !Set H to be the REAL value of the INTEGER HInt


	!Prompt user to input the normalized scalar flux in cm^-2 s^-1
	WRITE(*,*) "Please enter normalized scalar flux in cm^-2 s^-1: "
	READ(*,*) flux !Read normalized scalar flux in cm^-2 s^-1 from user
	
	
	!Prompt user to input 5 values for position along fuel rod axis in cm
	WRITE(*,*) "Please enter 5 values for position along fuel rod axis in cm: "
	READ(*,*) z1int,z2int,z3int,z4int,z5int !Read 5 values for position along fuel rod axis (cm) from user
	
	!Set z1-5 real values to that of their INT counterparts
	z1=REAL(z1int)
	z2=REAL(z2int)
	z3=REAL(z3int)
	z4=REAL(z4int)
	z5=REAL(z5int)
	
	!Check input data for errors
	!Ensure H>0
	IF(H<=0) nerr=nerr+1
	
	!Ensure flux>=0
	IF(flux<0) nerr=nerr+1
	
	!Ensure no two inputs were the same
	IF(z1==z2) nerr=nerr+1
	IF(z1==z3) nerr=nerr+1
	IF(z1==z4) nerr=nerr+1
	IF(z1==z5) nerr=nerr+1
	IF(z2==z3) nerr=nerr+1
	IF(z2==z4) nerr=nerr+1
	IF(z2==z5) nerr=nerr+1
	IF(z3==z4) nerr=nerr+1
	IF(z3==z5) nerr=nerr+1
	IF(z4==z5) nerr=nerr+1
	
	!Ensure all inputs are in bounds
	IF((z1<0).OR.(H<z1)) nerr=nerr+1
	IF((z2<0).OR.(H<z2)) nerr=nerr+1
	IF((z3<0).OR.(H<z3)) nerr=nerr+1
	IF((z4<0).OR.(H<z4)) nerr=nerr+1
	IF((z5<0).OR.(H<z5)) nerr=nerr+1
	
	!Stop if illegitimate data is detected
	IF(nerr>0) THEN
		WRITE(*,'(/,A,I1,A)') "Unfortunately your input had ",nerr," errors"
		STOP
	END IF
	
	
	!Write input data table
	WRITE(*,"(/,A23,A12,A23)") line," INPUT DATA ",line !Write 2 blank lines then header
	WRITE(*,"(A,T31,A,T36,E13.6,T49,A)") "Normalized thermal scalar flux","=",flux,"cm^-2 s^-1" !Show flux input
	WRITE(*,"(A,T31,A,T36,E13.6,T49,A)") "Length of fuel rod","=",H,"cm" !Show length input
	WRITE(*,"(A58)") line !Draw input data table bottom border

	
	!Set f1-5 values via provided equation, normalized flux, and corresponding z values
	f1=flux*COS(((PI*z1)/H)-(PI/2))
	f2=flux*COS(((PI*z2)/H)-(PI/2))
	f3=flux*COS(((PI*z3)/H)-(PI/2))
	f4=flux*COS(((PI*z4)/H)-(PI/2))
	f5=flux*COS(((PI*z5)/H)-(PI/2))
	
	
	!Calculate all respective macroscopic fission cross-sections
	IF((z1>(H/6)).AND.(z1<(5*H/6))) THEN
		E1=0.5
	ELSE
		E1=0.1
	END IF
	IF((z2>H/6).AND.(z2<5*H/6)) THEN
		E2=0.5
	ELSE
		E2=0.1
	END IF
	IF((z3>H/6).AND.(z3<5*H/6)) THEN
		E3=0.5
	ELSE
		E3=0.1
	END IF
	IF((z4>H/6).AND.(z4<5*H/6)) THEN
		E4=0.5
	ELSE
		E4=0.1
	END IF
	IF((z5>H/6).AND.(z5<5*H/6)) THEN
		E5=0.5
	ELSE
		E5=0.1
	END IF

	!Calculate powers using given equation
	p1=k*f1*E1
	p2=k*f2*E2
	p3=k*f3*E3
	p4=k*f4*E4
	p5=k*f5*E5
	
	
	!Write output data table
	WRITE(*,"(/,A21,A13,A21)") line,"   RESULTS   ",line
	WRITE(*,"(X,A,4X,A,4X,A)")"position (cm)", "Flux (cm^-2 s^-1)", "Power (W cm^-3)"
	372 FORMAT (X,E13.6,7X,E13.6,8X,E13.6) !Defines format for output table rows
	WRITE(*,372)z1,f1,p1
	WRITE(*,372)z2,f2,p2
	WRITE(*,372)z3,f3,p3
	WRITE(*,372)z4,f4,p4
	WRITE(*,372)z5,f5,p5
	WRITE(*,'(A54)') line
END PROGRAM APPLING_EXAM1