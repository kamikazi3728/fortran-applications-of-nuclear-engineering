PROGRAM APPLING_EXAM2

	!Author: Reece Appling
	!Date: 3/26/2021
	!Assignment Name: Exam 2
	!Assignment Description: Takes inputs to output fluxes at radial positions in a bare cylindrical reactor
	IMPLICIT NONE
	
	CHARACTER(60),PARAMETER::line='============================================================'
	CHARACTER(60)::outfile!Output file
	INTEGER::i,N!iteration variable, N steps
	REAL::fluxZero,stopCrit,R
	REAL,ALLOCATABLE::flux(:)
	REAL,EXTERNAL::Jo
	!------------------Variable Declaration---------------!
	!CHARACTER PARAMETERS
		!line - Horizontal borders for tables
	!INTEGER VARIABLES
		!hInt - Holds fuel rod length (cm)
		!z1int-z5int - Hold respective values for position along the fuel rod (cm)
		!nerr - number of errors found in input information, starts at 0
	!REAL VARIABLES
		!flux - Holds normalized scalar flux in cm^-2 s^-1
		!H - Holds fuel rod length (cm), REAL equivalent of hInt
		!z1-z5 - REAL equivalents of z1int-z5int respectively
		!f1-f5 - flux in cm^-2 s^-1 at positions z1-z5 respectively
		!E1-E5 - macroscopic fission cross-sections (/cm2 s), at positions z1-z5 respectively
		!p1-p5 - Powers in W/cm3 at positions z1-z5 respectively
	!REAL PARAMETERS
		!k - J/fission constant
		!PI - Mathematical constant PI
	!---------------End Variable Declaration--------------!

	!Read and verify inputs
	CALL INPUT(outfile,R,fluxZero,N,stopCrit,line)
	!Allocate arrays
	ALLOCATE(flux(N))
	DO i=1,N,1
		CALL CALCFLUX(i,N,R,fluxZero,flux(i),stopCrit)
	END DO
	!Output
	CALL OUTTOFILE(N,flux,outfile,line,R,fluxZero)
END PROGRAM APPLING_EXAM2

SUBROUTINE INPUT(outFileName,R,fluxZero,Nsteps,stopCrit,line)
	IMPLICIT NONE
	
	CHARACTER(60),INTENT(OUT)::outFileName
	INTEGER,INTENT(OUT)::Nsteps
	REAL,INTENT(OUT)::R,fluxZero,stopCrit
	CHARACTER(60),INTENT(IN)::line
	CHARACTER(60)::trashVar
	INTEGER::ioer,ioer2,nerr=0,i
	
	!Open file, read contents while echoing data, close file
	OPEN(UNIT=10, FILE='input', STATUS='OLD', ACTION='READ', IOSTAT=ioer)
	IF (ioer .NE. 0) STOP 'error opening ./input'
	READ(10,*,IOSTAT=ioer2)trashVar
	DO i=0,20
		IF (ioer2 .NE. 0) EXIT
		SELECT CASE(i)
			CASE(4)
				READ(10,*,IOSTAT=ioer2)outFileName
				WRITE(*,"(A23,A12,A23)") line," INPUT DATA ",line !Write header
				WRITE(*,'(2A)')"The output file name is: ",outFileName
			CASE(6)
				READ(10,*,IOSTAT=ioer2)R
				WRITE(*,'(A,F6.2,A)')"The radius of the bare reactor core is: ",R," cm"
			CASE(8)
				READ(10,*,IOSTAT=ioer2)fluxZero
				WRITE(*,'(A,E11.4,A)')"The normalized thermal scalar flux is: ",fluxZero," (cm-2 sec-1)"
			CASE(10)
				READ(10,*,IOSTAT=ioer2)Nsteps
				WRITE(*,'(A,I2)')"The number of radial positions: ",Nsteps
			CASE(12)
				READ(10,*,IOSTAT=ioer2)stopCrit
				WRITE(*,'(A,E11.4)')"The stopping criteria is: ",stopCrit
			CASE DEFAULT
				READ(10,*,IOSTAT=ioer2)
		END SELECT
	END DO
	CLOSE(UNIT=10)
	WRITE(*,"(A58)") line
	
	!Check all inputs, warn and add errors if necessary
	IF(R<=0) nerr = nerr+1;

	IF(fluxZero<=0) nerr = nerr+1;
	IF(Nsteps<=0) nerr = nerr+1;
	IF(stopCrit<=0) nerr = nerr+1;
	
	!if nerr >0, stop program, otherwise notify
	IF(nerr>0) THEN 
		STOP ": Program terminated due to input errors"
	ELSE
		WRITE(*,*) "Congratulations! There were no erroneous inputs."
	END IF
END SUBROUTINE INPUT

SUBROUTINE CALCFLUX(i,N,radMax,fo,fx,sc)
	IMPLICIT NONE
	
	INTEGER,INTENT(IN)::i,N
	REAL,INTENT(IN)::radMax,fo,sc
	REAL,INTENT(OUT)::fx
	REAL,EXTERNAL::Jo
	REAL::rad
	
	rad=(radMax/REAL(N))*(REAL(i))
	fx = fo*Jo((rad*2.4048)/radMax,sc)
END SUBROUTINE CALCFLUX





REAL FUNCTION factorial(num)
	IMPLICIT NONE
	INTEGER::num
	INTEGER::temp=1,i
	DO i=num,1,-1
		IF(num<2) EXIT
		temp = temp*i;
	END DO
	factorial = temp;
END FUNCTION




REAL FUNCTION Jo(x,sc)
	IMPLICIT NONE
	REAL::x
	INTEGER::m=0
	REAL,EXTERNAL::factorial
	REAL::tempSum=0,sc
	DO WHILE(SQRT(((1/(factorial(m)**2))*((x/2)**(2*m)))**2)<=sc)
		tempsum = tempSum+((((-1)**m)/(factorial(m)**2))*((x/2)**(2*m)));
	END DO
END FUNCTION

SUBROUTINE OUTTOFILE(nPoints,fluxes,outfile,line,R,fo)
	IMPLICIT NONE
	
	INTEGER,INTENT(IN)::nPoints
	REAL,INTENT(IN)::fluxes(nPoints),R,fo
	CHARACTER(30),INTENT(IN)::outfile
	CHARACTER(60),INTENT(IN)::line
	INTEGER::i,err1
	REAL::tempr
	OPEN(UNIT=15,FILE=outfile,STATUS='REPLACE',ACTION='WRITE',IOSTAT=err1)
	WRITE(*,"(A23,A9,A26)") line," RESULTS ",line !Write header
	WRITE(*,'(A,T20,A)')"Position(cm)","Flux(cm-2 s-1)"
	372 FORMAT (F5.9,T20,E13.6) !Defines format for output table rows
	WRITE(*,372)0.0000,fo
	DO i=1,nPoints
		tempr=(R/nPoints)*i
		WRITE(*,372)tempR,fluxes(i)
	END DO
	WRITE(*,"(A58)") line
	
END SUBROUTINE OUTTOFILE