Test case for 1D Diffusion Equation Solver                                      
----------------------------------------------------------------------
| Diffusion coefficient (cm)                     |    0.100000001    |
| Macroscopic Absorption cross section (/cm)     |    0.100000001    |
| Reactor Size (cm)                              |    4.000000000    |
| Neutron Source Strength (/cm)                  |    1.000000000    |
| Number of mesh cells                           |    8              |
----------------------------------------------------------------------
 Congratulations on entering proper input values. Code execution will now proceed

Solution:
          0    0.000000000
          1    3.710958242
          2    5.849655628
          3    6.950767517
          4    7.289570808
          5    6.950767517
          6    5.849656105
          7    3.710958004
          7    0.000000000
