PROGRAM APPLING_OUTLAB9

	!Author: Reece Appling
	!Date: 4/5/2021
	!Assignment Name: Outlab 9
	!Assignment Description: -------------------------------------------------------------------------------------------

	IMPLICIT NONE
	
	CHARACTER(70)::title
	CHARACTER(5),ALLOCATABLE::nuclide(:)
	INTEGER::steps,nNuclides
	REAL::maxTime
	REAL,ALLOCATABLE::info(:,:),lambda(:)
	!------------------Variable Declaration---------------!
	!CHARACTER VARIABLES
		!title - program case title
		!nuclide(:) - holds nuclide atomic symbols
	!INTEGER VARIABLES
		!steps - holds # of timesteps for a case
		!nNuclides - holds number of nuclides in a case
	!REAL VARIABLES
		!maxTime - maximum time for case (seconds)
		!info(:,:) holds number density values for all nuclides where row=nuclide,column=timestamp (column 1 is initial)
		!lambda(:) holds all decay constants for nuclides
	!---------------End Variable Declaration--------------!
	
	!Get file information, open files, read steps and number of nuclides
	CALL ARRDIM(steps,nNuclides)
	
	!Allocate arrays
	ALLOCATE(info(nNuclides,steps+1),nuclide(nNuclides),lambda(nNuclides))
	
	!Process all inputs, fill initial problem values from input file and check for errors
	CALL PROCESSINPUTS(title,nNuclides,maxTime,nuclide,lambda,info,steps)
	
	!Calculate all number densities at timesteps
	CALL DENSITIES(nNuclides,steps,maxTime,lambda,info)
	
	!Output results
	CALL RESULTS(nNuclides,steps,maxTime,lambda,info,title,nuclide)
END PROGRAM APPLING_OUTLAB9

!subroutine to get file names, read # timesteps and nuclides
SUBROUTINE ARRDIM(steps,nNuclides)
	IMPLICIT NONE
	INTEGER,INTENT(OUT)::steps,nNuclides
	CHARACTER(70)::fileIn,fileOut
	INTEGER::ioin,ioout
	!Ask for file names
	WRITE(*,*)"What is the name of the input file?"
	READ(*,*)fileIn
	WRITE(*,*)"What is the name of the output file?"
	READ(*,*)fileOut
	!Open input and output files
	OPEN(UNIT=11,FILE=fileIn,STATUS='OLD',ACTION='READ',IOSTAT=ioin)
	OPEN(UNIT=12,FILE=fileOut,STATUS='REPLACE',ACTION='WRITE',IOSTAT=ioout)
	!Handle file open errors
	IF (ioin .NE. 0) STOP 'error opening input file'
	IF (ioout .NE. 0) STOP 'error opening output file'
	!Read second input line for timesteps
	READ(11,*)
	READ(11,*)steps
	!Get number of nuclides
	nNuclides=0
	DO
	READ(11,*,IOSTAT=ioin)
		IF (ioin .NE. 0) EXIT
		nNuclides=nNuclides+1
	END DO
	REWIND(11)!Rewind input file
END SUBROUTINE

!subroutine to get all info from input file and ensure it is valid
SUBROUTINE PROCESSINPUTS(title,n,maxTime,nuclide,lambda,info,steps)
	IMPLICIT NONE
	CHARACTER(70),INTENT(OUT)::title
	INTEGER,INTENT(IN)::n,steps
	CHARACTER(5),INTENT(INOUT)::nuclide(n)
	INTEGER::timeSteps,i,nerr=0
	REAL,INTENT(OUT)::maxTime
	REAL,INTENT(INOUT)::lambda(n),info(n,steps+1)
	!read in title, timesteps, maxtime
	READ(11,'(A)')title
	READ(11,*)timeSteps,maxTime
	DO i=1,n
		READ(11,*)nuclide(i),info(i,1),lambda(i)
	END DO
	!Check validity of all inputs, count errors
	DO i=1,n
		IF(lambda(i)<=0) THEN
			nerr=nerr+1
			WRITE(*,*) "Error: lambda must be greater than 0"
		END IF
		IF(info(i,1)<0) THEN
			nerr=nerr+1
			WRITE(*,*) "Error: initial atomic density of A must not be negative"
		END IF
	END DO
	IF(maxTime<=0) THEN
		nerr=nerr+1
		WRITE(*,*) "Error: max time must be greater than 0"
	END IF
	IF(timeSteps<=0) THEN
		nerr=nerr+1
		WRITE(*,*) "Error: number of timesteps must be greater than 0"
	END IF
	!If any errors Advise and terminate
	IF(nerr>0) THEN
		WRITE(*,'(I2,A)')nerr," errors detected, terminating program"
		STOP
	END IF
	!If no errors then congratulate
	WRITE(*,*)"Congratulations, all inputs are valid. Continuing execution"
END SUBROUTINE

!Subroutine to calculate all number densities at all timesteps
SUBROUTINE DENSITIES(n,steps,maxTime,lambda,info)
	IMPLICIT NONE
	REAL,INTENT(IN)::maxTime
	INTEGER,INTENT(IN)::n,steps
	REAL,INTENT(INOUT)::lambda(n),info(n,steps+1)
	INTEGER::i,j
	REAL::dt,t,e=2.7182818
	
	!Caalculate timestep length
	dt=maxTime/REAL(steps)
	!Calculate every non-initial number density for each nuclide
	DO j=2,steps+1
		t=dt*(j-1)!Sets current time to (j-1)*dt
		DO i=1,n
			info(i,j)=info(i,1)*(e**(0-(lambda(i)*t)))
		END DO
	END DO
END SUBROUTINE

!Subroutine to output all results to output file
SUBROUTINE RESULTS(n,steps,maxTime,lambda,info,title,nuclide)
	IMPLICIT NONE
	CHARACTER(70),INTENT(IN)::title
	INTEGER,INTENT(IN)::n,steps
	CHARACTER(5),INTENT(IN)::nuclide(n)
	REAL,INTENT(IN)::lambda(n),info(n,steps+1),maxTime
	INTEGER::i,j
	REAL::dt
	
	!WRITE the title line as read from the input file
	WRITE(12,'(A,/,/)')title
	!Echo all input data in a professional style
	WRITE(12,'(A,I3)')"The entered number of time steps is: ",steps
	WRITE(12,'(A,E12.5)')"The entered final time is: ",maxTime
	WRITE(12,'(A,T14,A,T45,A)')"isotope","Initial number density(cm-3)","decay constant(s-1)"
	DO i=1,n
		WRITE(12,'(X,A,T15,E12.5,T46,E12.5)')nuclide(i),info(i,1),lambda(i)
	END DO
	WRITE(12,*)
	!WRITE solution header that contains the text “Time (sec)” then the radionuclide
		!symbols as read from the input file all on a single line
	WRITE(12,'(A)',ADVANCE='NO')"Time (sec)"
	DO j=1,n
		WRITE(12,'(15X,A5)',ADVANCE='NO')nuclide(j)
	END DO
	WRITE(12,'(A,/)',ADVANCE='NO')" "
	!Loop over all time steps and print one line per time step containing (under the
		!header described above) the time level followed by the atomic densities of all
		!radionuclides at that time level.
	dt=maxTime/REAL(steps)
	DO i=1,steps+1
		WRITE(12,'(E12.5)',ADVANCE='NO')(dt*(i-1))
		DO j=1,n
			WRITE(12,'(8X,E12.5)',ADVANCE='NO')info(j,i)
		END DO
		WRITE(12,'(A,/)',ADVANCE='NO')" "
	END DO
	!Conclude execution with a suggestive message.
	WRITE(12,'(A)')"Program executed and terminated successfully"
	WRITE(*,'(A)')"Program executed and terminated successfully"
END SUBROUTINE