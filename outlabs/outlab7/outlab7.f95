PROGRAM APPLING_OUTLAB7

	!Author: Reece Appling
	!Date: 4/05/2021
	!Assignment Name: Outlab 7
	!Assignment Description: Calculates fluxes and sources at discrete intervals utilizing arrays

	IMPLICIT NONE
	
	CHARACTER(70),PARAMETER::line='======================================================================'
	INTEGER::I,errs=0,r,c
	REAL::L,D,crs,flux,dx,tempSum
	REAL,ALLOCATABLE::phi(:),Q(:),A(:,:)
	!------------------Variable Declaration---------------!
	!CHARACTER PARAMETERS
		!line - Horizontal borders and dividers
	!INTEGER VARIABLES
		!I - number of computational cells
		!errs - Holds number of detected input errors, starts as 0
		!r - Holds iteration index for rows
		!c - Holds iteration index for columns
	!REAL VARIABLES
		!L - half of the reactor height
		!D - Diffusion coefficient
		!crs - absorption cross section
		!flux - uniform value of the flux at internal cells
		!dx - cell size
		!tempSum - temporary variable used when summing in loops
		!phi(:) - array of cell flux values from 1 to I-1
		!Q(:) - Array of Fixed source values in cells 1 to I-1
		!A(:,:) - I-1xI-1 Array A, where A*phi=Q
	!---------------End Variable Declaration--------------!
	
	!Displays Header Information
	WRITE(*,'(A60)') line
	WRITE(*,'(A)') "This code was developed by Reece Appling on Apr 5th 2021"
	WRITE(*,'(A60)') line
	
	!Prompts user to enter L, D, absorption crs, flux, and number of cells, and records all of them respectively
	WRITE(*,'(/,A)') "Please enter half of the reactor height L:"
	READ(*,*) L
	WRITE(*,'(A)') "Please enter the diffusion coefficient D:"
	READ(*,*) D
	WRITE(*,'(A)') "Please enter the absorption cross section:"
	READ(*,*) crs
	WRITE(*,'(A)') "Please enter the uniform value of the flux at internal cells:"
	READ(*,*) flux
	WRITE(*,'(A)') "Please enter the number of computational cells I:"
	READ(*,*) I

	!Echo input values
	WRITE(*,'(/,A27,A5,A28)') line,"INPUT",line
	WRITE(*,'(A,F12.8)')"Half of the reacator height L= ",L
	WRITE(*,'(A,F13.9)')"The diffusion coefficient D= ",D
	WRITE(*,'(A,E15.8)')"The absorption cross section is ",crs
	WRITE(*,'(A,F12.8)')"The uniform value of the flux at internal cells is ",flux
	WRITE(*,'(A,I4)')"The number of computational cells I=",I
	WRITE(*,'(A60,/)') line
	
	!Check for input errors
	IF(L<=0) errs=errs+1 !L must be greater than 0
	IF(D<=0) errs=errs+1 !D must be greater than 0
	IF(crs<=0) errs=errs+1 !Absorption cross section must be greater than 0
	IF(flux<0) errs=errs+1 !Flux cannot be less than 0
	IF(I<=0) errs=errs+1 !number of cells must be greater than 0
	
	!If there are any errors, notify and terminate
	IF(errs>0) THEN
		WRITE(*,'(A,I1,A)')"Error: ",errs," invalid inputs, please enter any key to terminate program execution"
		READ(*,*)
		STOP
	END IF
	
	!If the program has not been terminated, there are no errors, notify and continue
	WRITE(*,'(A)')"Congratulations! There are no mistakes in the input data."
	
	!Compute cell size
	dx = 2*L/REAL(I)

	!Allocate necessary arrays
	ALLOCATE(phi(I-1),Q(I-1),A(I-1,I-1))
	
	!loop over r=1,I-1 to set all internal fluxes to the entered flux
	DO r=1,I-1
		phi(r)=flux
	END DO
	
	!Loop over all columns in all rows to produce 2-D Array A
	DO r=1,I-1
		DO c=1,I-1
			IF(r.EQ.c) THEN
				A(r,c)=(((2*D)/(dx**2))+crs)
			ELSE IF((c.EQ.(r-1)).OR.(c.EQ.(r+1))) THEN
				A(r,c)=(0-(D/(dx**2)))
			ELSE
				A(r,c)=0
			END IF
		END DO
	END DO
	
	!Calculate Q by multiplying matrix A by phi
	DO r=1,I-1
		tempSum=0 !Set the temp sum to 0 to start for each "row"
		DO c=1,I-1
			tempSum=tempSum+(A(c,r)*phi(r))
		END DO
	
		Q(r)=tempSum !set Q(r) to be the sum calculated above
	END DO
	
	!Output calculated data
	WRITE(*,'(/,A26,A7,A27)') line,"RESULTS",line
	WRITE(*,'(3X,A,4X,A,7X,A,11X,A)')"i","Coordinate","Flux","Fixed Source"
	DO r=1,I-1
		WRITE(*,'(I4,4X,F8.4,5X,E13.6,5X,E13.6)')r,((0-L)+(dx*r)),phi(r),Q(r)
	END DO
	WRITE(*,'(A60,/)') line
END PROGRAM APPLING_OUTLAB7